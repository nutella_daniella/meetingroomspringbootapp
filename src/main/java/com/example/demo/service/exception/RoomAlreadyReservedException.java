package com.example.demo.service.exception;

public class RoomAlreadyReservedException extends Exception {

    public RoomAlreadyReservedException(String message) {
        super(message);
      }
    
}
