package com.example.demo.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.model.Booking;
import com.example.demo.repository.BookingRepository;
import com.example.demo.service.BookingService;
import com.example.demo.service.exception.RoomAlreadyReservedException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class BookingServiceImpl implements BookingService {
	
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private BookingRepository repository;

	@Override
	//@Transactional(rollbackFor = RoomAlreadyReservedException.class)
	public void register(Booking booking) throws RoomAlreadyReservedException {
		
		logger.info("Reserving: {}", booking);
		
	    try {
	        repository.save(booking);
	      } catch (RuntimeException duplicate) { 
	        throw new RoomAlreadyReservedException("The room is already reserved:" + booking.getRoomNumber());

	      }
		
	}

}
