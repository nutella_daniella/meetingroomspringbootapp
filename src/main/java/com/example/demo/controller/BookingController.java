<<<<<<< HEAD
package com.example.demo.controller;
=======
package org.bodotours.controller;
>>>>>>> ac3b17bf0d06272efc8047489b0a51f119e9902a

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

<<<<<<< HEAD
import com.example.demo.service.exception.RoomAlreadyReservedException;
=======
>>>>>>> ac3b17bf0d06272efc8047489b0a51f119e9902a
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.demo.model.Booking;
import com.example.demo.service.BookingService;

@Controller
public class BookingController {

  private static final String BOOKING_FORM = "booking";

  private static final String BOOKING_FORM_ATTRIBUTE = "bookingForm";

  private static final String PATH_BOOKING = "/booking";

  private final Logger logger = LoggerFactory.getLogger(getClass());
  
  
  
  @Autowired
<<<<<<< HEAD
  private BookingService bookingService;
  
  @GetMapping(value = {"/", PATH_BOOKING})
  public String getBookingForm(@ModelAttribute(BOOKING_FORM_ATTRIBUTE) Booking r) {
=======
  private BookingService registrationService;
  
  @GetMapping(value = {"/", PATH_BOOKING})
  public String getRegistrationForm(@ModelAttribute(BOOKING_FORM_ATTRIBUTE) Booking r) {
>>>>>>> ac3b17bf0d06272efc8047489b0a51f119e9902a
    return BOOKING_FORM;
  }
  
  @PostMapping(PATH_BOOKING)
<<<<<<< HEAD
  public String SubmitReservation(@ModelAttribute(BOOKING_FORM_ATTRIBUTE) @Valid Booking booking,
=======
  public String SubmitRegistration(@ModelAttribute(BOOKING_FORM_ATTRIBUTE) @Valid Booking booking,
>>>>>>> ac3b17bf0d06272efc8047489b0a51f119e9902a
      BindingResult result, HttpServletResponse response) {
    
    logger.debug("user submitted this reservation: {}", booking);
    
    String targetView = BOOKING_FORM;
    
    if (result.hasErrors()) {
      response.setStatus(HttpStatus.BAD_REQUEST.value());
      result.reject("bookingForm.error.incompleteInput");
      return BOOKING_FORM;
    } else {
      try {
<<<<<<< HEAD
        bookingService.register(booking);
=======
        BookingService.booking(booking);
>>>>>>> ac3b17bf0d06272efc8047489b0a51f119e9902a
        targetView = "redirect:/";
      } catch (RoomAlreadyReservedException e) {
        result.reject("error.reservationError.room.reserved");
      }
    }
    
    return targetView;
    
  }
  
  @ExceptionHandler
  @ResponseStatus(HttpStatus.CONFLICT)
  public String reservationAlreadyExists(RoomAlreadyReservedException e) {
    return "errors/error-reservation-already-exists";
  }
  

  
  
}
