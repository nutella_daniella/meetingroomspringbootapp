package com.example.demo.repository.impl;

import com.example.demo.model.Booking;
import com.example.demo.repository.BookingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.UUID;


@Repository
public class BookingRepositoryImpl implements BookingRepository {

    private Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private JdbcTemplate jdbcTemplate;



    @Override
    public void save(Booking booking) {

        logger.info("Save now" +booking);


        final String sql = "INSERT into reservations(name, roomNumber) values(?, ?)";

        final String id = UUID.randomUUID().toString();

        jdbcTemplate.update(sql, id, booking.getName(), booking.getRoomNumber());


    }
}
