package com.example.demo.repository;

import com.example.demo.model.Booking;
import org.springframework.stereotype.Component;

//@Component
public interface BookingRepository {

	void save(Booking booking);
}
