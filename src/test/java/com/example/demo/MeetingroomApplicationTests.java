package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.model.Booking;
import com.example.demo.service.BookingService;
import com.example.demo.service.exception.RoomAlreadyReservedException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MeetingroomApplicationTests {
	
	@Autowired
	private BookingService service;

	@Test
	public void test_bookingValidReservation_successful() throws RoomAlreadyReservedException{
		service.register(new Booking("Daniella", 12));
	}

}
